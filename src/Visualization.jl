#=
 * File: Visualization.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 25th April 2020 2:38:17 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module Visualization

#region imports
import PyPlot, PyCall
using ProgressMeter
using ..MetricTools

const MPatch = PyCall.PyNULL()
const MCollections = PyCall.PyNULL()
const MAnimations = PyCall.PyNULL()

"""
Called when the module is loaded, allows PyPlot imports to be precompiled correctly
"""
function __init__()
    copy!(MPatch, PyCall.pyimport("matplotlib.patches"))
    copy!(MCollections, PyCall.pyimport("matplotlib.collections"))
    copy!(MAnimations, PyCall.pyimport("matplotlib.animation"))
end
#endregion imports

export compilemp4, animate, build_matrix, build_matrix_pts

#region utils
"""
Patches a polygon so it may be plotted
"""
function patch(polygon::Polygon)::PyCall.PyObject
    patched = MPatch.Polygon(
        polygon,
        closed=true,
        edgecolor="black",
        facecolor="grey",
        rasterized=true
    )
    return patched
end

"""
Creates an animation
"""
function animate(fig::PyPlot.Figure, func::Function)
    # Create the animation object by calling the Python function FuncAnimaton
    myanim = MAnimations.FuncAnimation(fig, func, frames=100, interval=20)

    # Convert it to an MP4 movie file and saved on disk in this format.
    myanim.save("test.mp4", bitrate=-1, extra_args=["-vcodec", "libx264", "-pix_fmt", "yuv420p"])
end

"""
Compiles an MP4 out of a directory of images
"""
function compilemp4(dir::String;
        framerate=10::Integer,
        format="image2"::String,
        resolution="1920x1080"::String,
        iformat="img%04d.png"::String,
        vcodec="libx264"::String,
        crf=4::Integer,
        pixformat="yuv420p"::String,
        outdir="data/output/videos",
        outfile="out.mp4"
        )
    for d in (dir, outdir)
        if last(d) != '/' d = d * '/' end
    end
    println(dir, " ", outdir)
    
    run(`ffmpeg -y
        -r $framerate
        -f $format
        -s $resolution
        -i $dir$iformat
        -vcodec $vcodec
        -crf $crf
        -pix_fmt $pixformat
        -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2"
        $outdir$outfile`
    )
end

"""
Transforms a function from f(point::Point) to pf(x, y)
"""
function MetricTools.plotfunc(func::Function)
    return (x, y) -> func(convert(Point, [x, y]))
end

"""
Sorts a polygon by angle for plotting
"""
function sortpoly(polygon::Polygon)::Polygon
    center::Point = sum(polygon) ./ length(polygon)
    anglef(p::Point)::Float64 = atan((p-center)...)
    return sort(polygon; by=anglef)
end
#endregion utils

#region plots
#region geometric
"""
Plots a list of polygons
"""
function MetricTools.plot(
        polygons::Array{Polygon};
        ax=PyCall.PyNULL()::PyCall.PyObject,
        facecolor::String="grey",
        edgecolor::String="black",
        alpha::Float64=1.,
        zorder::Integer=2,
        points::Bool=false)

    # sortedpolys::Array{Polygon} = [sortpoly(p) for p in polygons] ##This will produce error
    sortedpolys::Array{Polygon} = polygons
    
    patches = [patch(polygon) for polygon in sortedpolys]
    patch_collection = MCollections.PatchCollection(patches, facecolor=facecolor, edgecolor=edgecolor, zorder=zorder, alpha=alpha)
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    collection = ax.add_collection(patch_collection)

    if points
        for polygon in polygons
            plot(polygon; ax=ax, zorder=zorder)
        end
    end
end

"""
Plots a list of polygon2Ds
"""
function MetricTools.plot(polygons2D::Array{Polygon2D}; ax=PyCall.PyNULL()::PyCall.PyObject)
    polygons::Array{Polygon} = [[
        [i for i in point2D] for point2D in poly2D
        ] for poly2D in polygons2D]
    plot(polygons; ax=ax)
end

"""
Plots a point
"""
function MetricTools.plot(
        point::Point;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        color="grey"::String,
        zorder=3::Integer,
        s=40::Integer)
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    ax.scatter([point[1]], [point[2]], color=color, edgecolors="black", zorder=zorder, s=s)
end

"""
Plots a list of points
"""
function MetricTools.plot(
        points::Array{Point};
        ispath::Bool=false,
        hasangles::Bool=false,
        ax::PyCall.PyObject=PyCall.PyNULL(),
        color::Union{String, Tuple}="grey",
        zorder::Integer=3,
        s::Integer=40,
        linestyle::String="solid",
        alpha::Float64=1.)
    if isempty(points) return end
    x, y = [p[1] for p in points], [p[2] for p in points]
        
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    ax.scatter(x, y, color=color, edgecolors="black", zorder=zorder, s=s, alpha=alpha)
    
    if hasangles
        thetas::Array{Float64} = [p[3] for p in points]
        u::Array{Float64} = cos.(thetas)
        v::Array{Float64} = sin.(thetas)
        ax.quiver(x, y, u, v, angles="uv", color=color)
    end
    
    if ispath
        lines::Array{Line} = 
            [(points[i], points[i + 1]) for i in firstindex(points):lastindex(points)-1]
        plot(lines;
            ax=ax, color=color,
            zorder=zorder, linestyle=linestyle,
            alpha=alpha)
    end
end

"""
Plots a line
"""
function MetricTools.plot(
        line::Line;
        ax::PyCall.PyObject=PyCall.PyNULL(),
        color::Union{String, Tuple}="grey",
        zorder::Integer=2,
        alpha::Float64=1.,
        linestyle::String="solid",
        width::Integer=1)
    x, y = [p[1] for p in line], [p[2] for p in line]
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    ax.plot(x, y; color=color, zorder=zorder, alpha=alpha,
        linestyle=linestyle, linewidth=width)
end

"""
Plots an array of lines
"""
function MetricTools.plot(
        lines::Array{Line};
        ax::PyCall.PyObject=PyCall.PyNULL(),
        color::Union{String, Tuple}="grey",
        zorder::Integer=2,
        vertices::Bool=false,
        alpha::Float64=1.,
        linestyle::String="solid",
        width::Integer=1,
        progress::Bool=true)
    

    if progress
        @showprogress for line in lines
            if vertices plot(first(line); ax=ax, color=color, zorder=zorder) end
            plot(line;
                ax=ax, color=color, zorder=zorder,
                alpha=alpha, linestyle=linestyle, width=width)
        end
    else   
        for line in lines
            if vertices plot(first(line); ax=ax, color=color, zorder=zorder) end
            plot(line;
                ax=ax, color=color, zorder=zorder,
                alpha=alpha, linestyle=linestyle, width=width)
        end
    end
    
    if vertices plot(last(last(lines));
        ax=ax, color=color, zorder=zorder,
        alpha=alpha, linestyle=linestyle, width=width)
    end
end

"""
Plots a circle
"""
function MetricTools.plot(
        c::Point,
        r::Float64;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        color="black"::String,
        fill=false::Bool,
        alpha=1.0::Float64,
        zorder=3::Integer)

    circle::PyCall.PyObject = MPatch.Circle(c, r, color=color, fill=fill, alpha=alpha, zorder=zorder)
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    ax.add_artist(circle)
end
#endregion geometric


#region functions
"""
Gets the range of x and y values for the matrix for plotting 2D functions
"""
function get_matrix_xys(
        length::Integer,
        bounds::Array{Bound}
        )::Tuple{StepRangeLen{Float64}, StepRangeLen{Float64}}
    
    xmin::Float64, xmax::Float64 = bounds[1].lo, bounds[1].hi
    ymin::Float64, ymax::Float64 = bounds[2].lo, bounds[2].hi
    xstep::Float64 = (bounds[1].hi - bounds[1].lo) / length
    ystep::Float64 = (bounds[1].hi - bounds[1].lo) / length
        
    return xmin:xstep:xmax, ymin:ystep:ymax
end
    
"""
Builds a matrix for plotting a 2D function
"""
function build_matrix(
        plotfunc1::Function,
        length::Integer,
        bounds::Array{Bound}
        )::Matrix{Union{Float64, Nothing}}

    X::StepRangeLen{Float64}, Y::StepRangeLen{Float64} =
        get_matrix_xys(length, bounds)
    return @showprogress [plotfunc1(x, y) for y in Y, x in X]
end

"""
Builds matrix points
"""
function build_matrix_pts(
    length::Integer,
    bounds::Array{Bound}
    )::Array{Point}
    
    X::StepRangeLen{Float64}, Y::StepRangeLen{Float64} =
        get_matrix_xys(length, bounds)
    return Point[[x, y] for y in Y, x in X]
end

"""
Plots the contour of a 2D function
"""
function MetricTools.plot(
        func1::Function;
        ax::PyCall.PyObject=PyCall.PyNULL(),
        length::Integer=100,
        bounds::Array{Bound}=[Bound(0, 1), Bound(0, 1)],
        flevels::Integer=50,
        cbar::Bool=true,
        cmin::Float64=0.,
        cmax::Float64=0.,
        cnuml::Integer=10,
        clevels::Array{Float64}=[i/cnuml for i in 1:round(cmax * cnuml)],
        calpha::Float64=0.5)
        
    plotfunc1::Function = plotfunc(func1)

    X::StepRangeLen{Float64}, Y::StepRangeLen{Float64} =
        get_matrix_xys(length, bounds)
    z::Matrix{Union{Float64, Nothing}} =
        @showprogress [min(plotfunc1(x, y), cmax) for y in Y, x in X]
    
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    
    # if no specified range
    if cmin == cmax
        cpf = ax.contourf(X, Y, z, levels=flevels, zorder=0)
        if !isempty(clevels)
            cp = ax.contour(X, Y, z,
                levels=clevels,
                colors="black",
                alpha=calpha,
                zorder=1)
        end
    else
        cpf = ax.contourf(X, Y, z,
            levels=flevels, zorder=0, vmin=cmin, vmax=cmax)
        if !isempty(clevels)
            cp = ax.contour(X, Y, z,
                levels=clevels,
                colors="black",
                alpha=calpha,
                zorder=1,
                vmin=cmin,
                vmax=cmax)
        end
    end
    
    if cbar
        cbar = PyPlot.colorbar(cpf, ax=ax)
    end
end

"""
Plots the error between two functions
"""
function MetricTools.plot(
        func1::Function,
        func2::Function;
        ax::PyCall.PyObject=PyCall.PyNULL(),
        signed::Bool=false,
        bounds::Array{Bound}=[Bound(0, 1), Bound(0, 1)],
        length::Integer=100,
        flevels::Integer=50,
        cmin::Float64=0.,
        cmax::Float64=0.,
        cnuml::Integer=10,
        clevels::Array{Float64}=[i/cnuml for i in 1:round(cmax * cnuml)],
        calpha::Float64=0.5)
        
    operator::Function = (v1, v2) -> abs(v2 - v1)
    if signed operator = (v1, v2) -> v2 - v1 end
    
    function errfunc(p) 
        v1, v2 = func1(p), func2(p)
        if isnothing(v1) || isnothing(v2) return nothing end
        return operator(v1, v2)
    end
    plot(errfunc; ax=ax, bounds=bounds, flevels=flevels,
        clevels=clevels, length=length, cmin=cmin, cmax=cmax, cnuml=cnuml, calpha=calpha)
end
#endregion functions
#endregion plots

end
