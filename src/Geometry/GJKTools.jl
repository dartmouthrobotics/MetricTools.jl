#=
 * File: GJK.jl
 * Project: Geometry
 * File Created: Wednesday, 18th March 2020 2:17:35 pm
 * Author: Josiah Putman (joshikatsu@gmail.com); Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
 * -----
 * Last Modified: 4th Nov 2020 
 * Modified By: Luyang Zhao (luyang.zhao.gr@dartmouth.com)
=#

module GJKTools

using EnhancedGJK
using StaticArrays
using MetricTools

export gjkdist, penetratedepth, gjkdist_with_pt, penetratedepth_with_pt

"""
Computes min distance between polygon and points
"""
function gjkdist_with_pt(polygon::Polygon, point::Point)::Point#Tuple{Float64, Point}
    N::Integer = length(polygon)
    spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2, Float64}(point)
    result::GJKResult = gjk(spoint, spolygon)
    cpoint::Point = []
    # if result.in_collision
    #     return -0.1, cpoint
    # else
    #     cpoint = [result.closest_point_in_body.b...]  #This cpoint is the closest_point on obstacle
    #     return separation_distance(result), cpoint
    # end
    if result.in_collision return cpoint end
    return [result.closest_point_in_body.b...]
end

"""
Computes min distance between polygons and point
"""
function gjkdist_with_pt(polygons::Array{Polygon}, point::Point)::Array{Point}#Tuple{Array{Float64}, Array{Point}}
    # println("checking point $point againist polygons $polygons...")
    # mindist::Float64 = typemax(Float64)
    # all_dist::Array{Float64} = []
    all_points::Array{Point} = []
    for polygon in polygons
        #dist::Float64, cpoint::Point = gjkdist(polygon, point)
        cpoint::Point = gjkdist_with_pt(polygon, point)
        if isempty(cpoint) 
            temp::Array{Point} = []
            return temp
        end
        if !(cpoint in all_points) 
            # push!(all_dist, dist)
            push!(all_points, cpoint)
        end
    end
    # mindist::Float64 = minimum(all_dist)
    # indices = findall(x -> isapprox(x, mindist), all_dist)
    # closest_points::Array{Point} = [all_points[i] for i in indices] 
    return all_points

end

"""
Computes min distance between polygon and points
"""
function gjkdist(polygon::Polygon, point::Point)::Float64 #Tuple{Float64, Point}
    N::Integer = length(polygon)
    spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2, Float64}(point)
    result::GJKResult = gjk(spoint, spolygon)
    # cpoint::Point = [result.closest_point_in_body.b...]  #This cpoint is the closest_point on obstacle
    # return separation_distance(result), cpoint
    if result.in_collision return -0.1 end
    return separation_distance(result)
end

"""
Computes min distance between polygons and point
"""
function gjkdist(polygons::Array{Polygon}, point::Point)::Float64 #Tuple{Float64, Array{Point}}
    # println("checking point $point againist polygons $polygons...")
    mindist::Float64 = typemax(Float64)
    # all_dist::Array{Float64} = []
    # all_points::Array{Point} = []
    for polygon in polygons
        dist::Float64 = gjkdist(polygon, point)
        # dist::Float64, cpoint::Point = gjkdist(polygon, point)
        # if !(cpoint in all_points) 
        #     push!(all_dist, dist)
        #     push!(all_points, cpoint)
        # end
        if dist < 0 return -0.1 end
        if dist < mindist mindist = dist end
    end
    # mindist::Float64 = minimum(all_dist)
    # indices = findall(x -> isapprox(x, mindist), all_dist)
    # closest_points::Array{Point} = [all_points[i] for i in indices] 
    ###! closest_points are all points with minimum distance
    # return mindist, closest_points
    return mindist

end

"""
Computes min distance between polygon and line
"""
function gjkdist(polygon::Polygon, line::Line)::Float64
    N::Integer = length(polygon)
    spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2}([SVector{2, Float64}(p) for p in line])
    result::GJKResult = gjk(spoint, spolygon)

    if result.in_collision 
        return -0.1 
    end
    return separation_distance(result)
end

"""
Computes min distance between polygons and line
"""
function gjkdist(polygons::Array{Polygon}, line::Line)::Float64
    mindist::Float64 = typemax(Float64)
    for polygon in polygons
        dist::Float64 = gjkdist(polygon, line)
        if dist < 0 return -0.1 end
        if dist < mindist mindist = dist end
    end
    
    return mindist
end

"""
Computes min distance between polygons and lines
"""
function gjkdist(polygons::Array{Polygon}, lines::Array{Line})::Float64
    mindist::Float64 = typemax(Float64)
    for line in lines
        dist::Float64 = gjkdist(polygons, line)
        if dist < 0 return -0.1 end
        if dist < mindist mindist = dist end
    end
    return mindist
end

"""
Computes max penetration distance between polygon and points
"""
function penetratedepth_with_pt(polygon::Polygon, point::Point)::Point
    N::Integer = length(polygon)
    spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2, Float64}(point)
    result::GJKResult = gjk(spoint, spolygon)
    if !result.in_collision return [] end
    println("current point is: ", point, " polygon is: ", polygon)
    println("the closet point is: ")
    println(simplex_penetration_distance(result))
    # println([closest_point_in_world(result)])
    return [closest_point_in_world(result)]  ## this doesn't work
    # return 
end

"""
Computes max penetration distance between polygons and point
"""
function penetratedepth_with_pt(polygons::Array{Polygon}, point::Point)::Array{Point} #Tuple{Float64, Array{Point}}
    # println("checking point $point againist polygons $polygons...")
    minpoints::Array{Point} = []
    for polygon in polygons
        pt::Point = penetratedepth_with_pt(polygon, point)
        if !isempty(pt) push!(minpoints, pt) end
    end
    return minpoints

end

"""
Computes max penetration distance between polygon and points
"""
function penetratedepth(polygon::Polygon, point::Point)::Float64 #Tuple{Float64, Point}
    N::Integer = length(polygon)
    spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2, Float64}(point)
    result::GJKResult = gjk(spoint, spolygon)
    if !result.in_collision return 0.0 end
    return simplex_penetration_distance(result)
end

"""
Computes max penetration distance between polygons and point
"""
function penetratedepth(polygons::Array{Polygon}, point::Point)::Float64 #Tuple{Float64, Array{Point}}
    # println("checking point $point againist polygons $polygons...")
    mindist::Float64 = typemin(Float64)
    for polygon in polygons
        dist::Float64 = penetratedepth(polygon, point)
        if dist > mindist mindist = dist end
    end
    return mindist

end

"""
Computes max penetration distance between polygon and line
"""
function penetratedepth(polygon::Polygon, line::Line)::Float64
    N::Integer = length(polygon)
    spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2}([SVector{2, Float64}(p) for p in line])
    result::GJKResult = gjk(spoint, spolygon)
    if !result.in_collision return 0.0 end
    return simplex_penetration_distance(result)
end

"""
Computes max penetration distance between polygons and line
"""
function penetratedepth(polygons::Array{Polygon}, line::Line)::Float64
    mindist::Float64 = typemin(Float64)
    for polygon in polygons
        dist::Float64 = penetratedepth(polygon, line)
        if dist > mindist 
            mindist = dist 
        end
    end
    return mindist
end

"""
Computes max penetration distance between polygons and lines
"""
function penetratedepth(polygons::Array{Polygon}, lines::Array{Line};)::Float64
    mindist::Float64 = typemin(Float64)
    for line in lines
        dist::Float64 = penetratedepth(polygons, line)
        if dist > mindist mindist = dist end
    end
    return mindist
end


# """
# Computes min penetration distance between polygons and lines
# """
# function penetratedepth2(polygons::Array{Polygon}, lines::Array{Line})::Float64
#     mindist::Float64 = typemax(Float64)
#     for polygon in polygons
#         dist::Float64 = penetratedepth2(polygon, lines)
#         if dist < mindist mindist = dist end
#     end
#     return mindist
# end


# """
# Computes min distance between polygon and line
# """
# function penetratedepth2(polygon::Polygon, lines::Array{Line})::Float64
#     N::Integer = length(polygon)
#     spolygon::SVector{N} = SVector{N}([SVector{2, Float64}(p) for p in polygon])
#     n_line::Int64 = length(lines)+1
#     line_points::Array{Point} = [lines[1]...]
#     for i in 2:length(lines)
#         push!(line_points, lines[i][2])
#     end
#     spoint::SVector{n_line} = SVector{n_line}([SVector{2, Float64}(p) for p in line_points])
#     result::GJKResult = gjk(spoint, spolygon)
#     # cpoint::Point = [result.closest_point_in_body.a...]
#     return simplex_penetration_distance(result))
# end



end
