#=
 * File: EarClipping.jl
 * Project: src
 * File Created: Wednesday, 18th March 2020 11:13:51 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 18th March 2020 11:36:50 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module EarClipping

using MetricTools, MetricTools.Geometry
import LinearAlgebra: norm, dot

export isear, earclip

"""
Gets three consecutive points (u, v, w) of a polygon, with v at index i
"""
function getuvw(polygon::Polygon, i::Integer)::Tuple{Point, Point, Point}
    n::Integer = length(polygon)
    u::Point = i - 1 >= firstindex(polygon) ? polygon[i - 1] : last(polygon)
    v::Point = polygon[i]
    w::Point = i + 1 < lastindex(polygon) ? polygon[i + 1] : first(polygon)
    return u, v, w
end

"""
Checks if v, with neighboring vertices u and w, is an ear of the polygon
"""
function isear(polygon::Polygon,i::Integer)::Bool
    u::Point, v::Point, w::Point = getuvw(polygon, i)
    uv::Point = u - v
    wv::Point = w - v
    
    theta::Float64 = acos(dot(uv, wv) / (norm(uv) * norm(wv)))
    contained::Bool = contains(polygon, (u, w); onedge=true)
    return theta < pi && contained
end

"""
Performs the ear clipping algorithm on the polygon
"""
function earclip(polygon::Polygon; if_reverse::Bool=false)::Array{Polygon}
    clipped::Array{Polygon} = []
    polygon = if_reverse ? reverse(polygon) : polygon
    while length(polygon) > 3
        changed::Bool = false
        for i in eachindex(polygon)
            if isear(polygon, i)
                push!(clipped, collect(getuvw(polygon, i)))
                deleteat!(polygon, i)
                changed = true
                break
            end
        end
        if !changed break end
    end

    push!(clipped, polygon)
    return clipped
end

end
