#=
 * File: Geometry.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 20th March 2020 3:30:16 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module Geometry

#region imports
import LinearAlgebra
import JSON2

using ..MetricTools
#endregion imports

export det3p, contains, intersects, intersection, load_polygons, iscolinear, midpoint

@enum Orientation begin 
    colinear=1
    clockwise=2
    counterclockwise=3
    unknown=4
end

const X = 1
const Y = 2

#region contains
"""
Returns true point p is on line l
If allow on edge, then a point on the end of the line counts as inside
"""
function MetricTools.contains(l::Line, p::Point; o=unknown::Orientation, onedge=true::Bool)
    if o == unknown o = orientation(l[1], p, l[2]) end
    if o != colinear return false end
    maxdim::Integer = min(length(first(l)), length(p))

    # check each dimension, make sure the point is in the range
    for (d, v) in enumerate(p)
        if d > maxdim continue end
        
        # if out of bounds, then this point is not in the segment
        # if on edge, this does not violate containment
        if v < min(l[1][d], l[2][d]) || v > max(l[1][d], l[2][d])
            return false
        end
        
        # if being on the edge counts as inside 
        if !onedge && (v == min(l[1][d], l[2][d]) || v == max(l[1][d], l[2][d]))
            return false
        end
    end
    return true
end

"""
Returns true if l2 is on l1
If allow on edge, then a point on the end of the line counts as inside
"""
function MetricTools.contains(l1::Line, l2::Line; o=unknown::Orientation, onedge=true::Bool)
    return (contains(l1, l2[1]; onedge=onedge) && contains(l1, l2[2]; onedge=onedge))
end

"""
From Luxor.jl
    isinside(p, pol; onedge=false)
Is a point `p` inside a polygon `pol`? Returns true if it does, or false.
This is an implementation of the Hormann-Agathos (2001) Point in Polygon algorithm.
The classification of points lying on the edges of the target polygon, or coincident with its vertices is not clearly defined, due to rounding errors or arithmetical inadequacy.
"""
function MetricTools.contains(polygon::Array{Point}, p::Point; onedge=true::Bool)::Bool
    n::Integer = length(polygon)
    
    ponedge = false
    c = false

    for i in 1:n
        q1, q2 = polygon[i], polygon[i % n + 1]
        
        # handles edge case
        q1q2::Line = (q1, q2)
        if contains(q1q2, p) return onedge end

        if (q1[2] < p[2]) != (q2[2] < p[2]) # crossing
            if q1[1] >= p[1]
                if q2[1] > p[1]
                    c = !c
                elseif ((det3p(q1, q2, p) > 0) == (q2[2] > q1[2]))
                    c = !c
                end
            elseif q2[1] > p[1]
                if ((det3p(q1, q2, p) > 0) == (q2[2] > q1[2]))
                    c = !c
                end
            end
        end
    end
    
    return c
end

"""
Determines if a line lies compeltely inside of a polygon
"""
function MetricTools.contains(polygon::Polygon, line::Line; onedge=false)::Bool
    # if the start and end point are not both inside, not contained
    if !(contains(polygon, line[1]; onedge=onedge) && contains(polygon, line[2]; onedge=onedge))
        return false
    end

    # if any edges cross, not fully contained
    n::Integer = length(polygon)
    for i in 1:n
        pline::Line = (polygon[i], polygon[i % n + 1])
        if intersects(line, pline; onedge=false)
            return false
        end
    end

    # otherwise check the midpoint
    return contains(polygon, midpoint(line); onedge=onedge)
end
#endregion contains


#region intersects
"""
Returns true if two line segments intersects
"""
function intersects(l1::Line, l2::Line; onedge=true::Bool)::Bool
    # Find the four orientations needed for general and special cases 
    o1::Orientation = orientation(l1[1], l1[2], l2[1])
    o2::Orientation = orientation(l1[1], l1[2], l2[2]) 
    o3::Orientation = orientation(l2[1], l2[2], l1[1])
    o4::Orientation = orientation(l2[1], l2[2], l1[2])

    if !onedge && colinear in Set([o1, o2, o3, o4]) return false end

    # General case 
    if o1 != o2 && o3 != o4 return true end

    # Special Cases (l1 = p1q1, l2 = p2q2)
    # p1, q1 and p2 are colinear and p2 lies on segment l1
    if contains(l1, l2[1]; o=o1, onedge=onedge) return true end
  
    # p1, q1 and q2 are colinear and q2 lies on segment l1
    if contains(l1, l2[2]; o=o2, onedge=onedge) return true end
  
    # p2, q2 and p1 are colinear and p1 lies on segment l2 
    if contains(l2, l1[1]; o=o3, onedge=onedge) return true end
  
    # p2, q2 and q1 are colinear and q1 lies on segment l2 
    if contains(l2, l1[2]; o=o4, onedge=onedge) return true end
  
    return false;
end

"""
Returns true if a polygon intersects with a line
If onedge is true, then a line on the polygon edge counts as an intersect
"""
function intersects(polygon::Polygon, line::Line; onedge=true::Bool)
    n::Integer = length(polygon)
    for point in line
        if contains(polygon, point; onedge=onedge) return true end
    end

    if contains(polygon, midpoint(line); onedge=onedge) return true end
    
    intersectps::Set{Point} = Set{Point}(line)
    for i in 1:n
        pline::Line = (polygon[i], polygon[i % n + 1])
        intersectp::Union{Point, Nothing} = intersection(line, pline)
        if !isnothing(intersectp)
            # println("intersect for $line and $pline at $intersectp")
            if onedge return true end
            push!(intersectps, intersectp)
        end
    end
    
    if onedge return false end
    
    # check all midpoints between intersecting points in order of x value
    # unless they ahve the same x, in which case sort by y
    sortedintercectps::Array{Point} =
        line[1][1] == line[2][1] ?
        sort(collect(intersectps); by=(p) -> p[2]) :
        sort(collect(intersectps); by=(p) -> p[1])
        
    if [0.75, 0.7] in line && [0.85, 0.7] in line println("isps: $sortedintercectps") end
    m::Integer = length(sortedintercectps) - 1
    for i in 1:m
        midp::Point = midpoint((sortedintercectps[i], sortedintercectps[i + 1]))
        if [0.75, 0.7] in line && [0.85, 0.7] in line println("testing midp $midp") end
        if contains(polygon, midp; onedge=false) return true end
    end

    return false
end

"""
Returns true if a list of polygons intersects with a line
"""
function intersects(polygons::Array{Polygon}, line::Line; onedge=true::Bool)
    for polygon in polygons
        if intersects(polygon, line; onedge=onedge) return true end
    end
    
    return false
end
#endregion intersects


#region utils
"""
Calculates the midpoint
"""
function midpoint(l::Union{Line, Array{Point}})::Point
    return [(l[1][d] + l[2][d]) / 2 for d in 1:lastindex(l[1])]
end

"""
Returns true if two lines are colinear
"""
function iscolinear(l1::Line, l2::Line)::Bool
    return orientation(l1[1], l2[1], l1[2]) == colinear &&
        orientation(l1[1], l2[2], l1[2]) == colinear
end
"""
Calculates the slope of a line (2D)
"""
function slope(l::Line)::Float64
    return (l[2][Y] - l[1][Y])/(l[2][X] - l[1][X])
end

"""
Calculates the slope of a line (2D) defined by two points
"""
function slope(p1::Point, p2::Point)::Float64
    return (p2[Y] - p1[Y])/(p2[X] - p1[X])
end

"""
Returns the orientation of 3 points
"""
function orientation(p::Point, q::Point, r::Point)::Orientation
    val::Float64 = (q[Y] - p[Y]) * (r[X] - q[X]) - 
                (q[X] - p[X]) * (r[Y] - q[Y])
    if val == 0 return colinear end
    if val > 0 return clockwise end
    return counterclockwise
end

"""
Calculates the determinant of three 2D points
"""
function det3p(q1::Point, q2::Point, p::Point)::Float64
    (q1[1] - p[1]) * (q2[2] - p[2]) - (q2[1] - p[1]) * (q1[2] - p[2])
end

"""
Calcualtes the determinant of two 2D points
"""
function det2p(p1::Point, p2::Point)::Float64
    p1[1] * p2[2] - p1[2] * p2[1]
end

"""
Loads obstacles from a given file
"""
function load_polygons(filename::String)::Array{Polygon}
    obstacles2D = []
    if isempty(filename) return obstacles2D end
    open(filename) do file
        obstacles2D = JSON2.read(file, Array{Polygon2D})
    end
    obstacles::Array{Polygon} = [[[p2D[1], p2D[2]] for p2D in poly2D] for poly2D in obstacles2D]
    return obstacles
end

"""
Calculates the intersection between two 2D lines
"""
function intersection(l1::Line, l2::Line; verbose::Bool=false)::Union{Point, Nothing}    
    if l1[1] in l2 return l1[1] end
    if l1[2] in l2 return l1[2] end
    
    e::Float64 = 4 * eps(Float64)
    xmin::Float64 = -e + max(
        min([p[1] for p in l1]...),
        min([p[1] for p in l2]...))
    xmax::Float64 = e + min(
        max([p[1] for p in l1]...),
        max([p[1] for p in l2]...))
    ymin::Float64 = -e + max(
        min([p[2] for p in l1]...),
        min([p[2] for p in l2]...))
    ymax::Float64 = e + min(
        max([p[2] for p in l1]...),
        max([p[2] for p in l2]...))
        
    xdiff::Point = [l1[1][1] - l1[2][1], l2[1][1] - l2[2][1]]
    ydiff::Point = [l1[1][2] - l1[2][2], l2[1][2] - l2[2][2]]
    
    div::Float64 = det2p(xdiff, ydiff)
    if div == 0
       return nothing
    end
    
    d::Point = [det2p(l1...), det2p(l2...)]
    x::Float64 = det2p(d, xdiff) / div
    y::Float64 = det2p(d, ydiff) / div
    if !(xmin <= x <= xmax) || !(ymin <= y <= ymax) 
        if verbose println("($x, $y) out of range ($xmin, $xmax), ($ymin, $ymax)") end
        return nothing end
    return [x, y]
end
#endregion utils

end
