#=
File: MetricGraphs.jl
Project: MetricTools
File Created: Monday, 1st July 2019 11:55:21 am
Author: Josiah Putman (joshikatsu@gmail.com); Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
-----
Last Modified: 3rd Nov 2020 
Modified By: Luyang Zhao (luyang.zhao.gr@dartmouth.com)
=#

module MetricGraphs

#region imports
using DataStructures
import PyPlot, PyCall
import LightGraphs, MetaGraphs
import Distances

using ..MetricTools, ..MetricTools.KNNs
#endregion imports

export
    MetricGraph,
    get_vertex,
    get_vertices,
    get_point,
    get_neighbors,
    get_points,
    get_edges,
    knn,
    knnids,
    nnbuild!,
    get_lines,
    astar,
    changed_astar,
    lp_astar_initialize,
    updateNode,
    updateNode_opt,
    dijkstras,
    dijkstradist,
    dijkstrapath,
    floyd_warshall,
    pop_point!,
    degree,
    set_weight!,
    nv

"""
Defines a graph that is embedded in a metric space
"""
struct MetricGraph
    dim::Integer
    graph::MetaGraphs.MetaGraph
    pointids::Dict{Point, Integer}
    metric::Distances.Metric
    knn::KNNs.KNN
    ###!!! Made modification here to make initial weights to be Inf
    function MetricGraph(dim::Integer; metric=Distances.Euclidean(), knntype=KNNs.BallTreeKNN)
        return new(dim, MetaGraphs.MetaGraph(0, Inf), Dict(), metric, knntype(dim; metric=metric))
    end
end

nv(g::MetricGraph) = LightGraphs.nv(g.graph)

#region points
"""
Adds a point to the graph
"""
function MetricTools.add!(g::MetricGraph, point::Point; usenn=false::Bool)::Bool
    # if contains(g, point) return false end
    added::Bool = MetaGraphs.add_vertex!(g.graph)
    v::Integer = MetaGraphs.nv(g.graph)
    g.pointids[point] = v
    MetaGraphs.set_prop!(g.graph, v, :point, point)
    if usenn
        add!(g.knn, point, v)
    end
    return true
end

"""
Adds an array of points to the graph
"""
function MetricTools.add!(g::MetricGraph, points::Array{Point}; usenn=false::Bool)::Bool
    all_added::Bool = true
    for (i, point) in enumerate(points)
        if !add!(g, point; usenn=usenn) all_added = false end
    end
    return all_added
end

"""
Pops off the last point added
"""
function pop_point!(g::MetricGraph)
    # TODO
end

"""
Removes a point from the graph
This breaks the KNN structure
"""
function MetricTools.remove!(g::MetricGraph, point::Point)::Bool
    if !contains(g, point) return false end
    rv::Integer = get_vertex(g, point)
    
    removed::Bool = MetaGraphs.rem_vertex!(g.graph, rv)
    if !removed return false end

    delete!(g.pointids, point)
    # internally the removal is performed swapping the vertices v and |V|
    # so update our record for all of the pointids
    for (p, v) in g.pointids
        if v >= rv
            g.pointids[p] = v - 1
        end
    end
    return true
end

"""
Gets a point
"""
get_point(g::MetricGraph, v::Integer) =
    MetaGraphs.get_prop(g.graph, v, :point)

"""
Gets all points
"""
get_points(g::MetricGraph)::Array{Point} =
    [get_point(g, v) for v in MetaGraphs.vertices(g.graph)]
#endregion points


#region edges
"""
Adds an edge to the graph
"""
function MetricTools.add!(g::MetricGraph, edge::MetaGraphs.Edge)
    p1::Array = get_point(g, edge.src)
    p2::Array = get_point(g, edge.dst)
    
    MetaGraphs.add_edge!(g.graph, edge)
    MetaGraphs.set_prop!(g.graph, edge, :weight, g.metric(p1, p2))
end

"""
Sets the weight of an edge
"""
set_weight!(g::MetricGraph, edge::MetaGraphs.Edge, w::Float64) =
    MetaGraphs.set_prop!(g.graph, edge, :weight, w)
set_weight!(g::MetricGraph, p1::Point, p2::Point, w::Float64) =
    set_weight!(g, MetaGraphs.Edge(get_vertex(g, p1), get_vertex(g, p2)), w)

"""
Auxilary add functions
"""
MetricTools.add!(g::MetricGraph, vertex1::Integer, vertex2::Integer) =
    add!(g, MetaGraphs.Edge(vertex1, vertex2))
MetricTools.add!(g::MetricGraph, p1::Point, p2::Point) =
    add!(g, MetaGraphs.Edge(get_vertex(g, p1), get_vertex(g, p2)))
MetricTools.add!(g::MetricGraph, line::Line) =
    add!(g, MetaGraphs.Edge(get_vertex(g, line[1]), get_vertex(g, line[2])))
MetricTools.add!(g::MetricGraph, edges::Array{MetaGraphs.Edge}) =
    for edge in edges add!(g, edge) end
MetricTools.add!(g::MetricGraph, edges::Array{Array{Integer}}) =
    for edge in edges add!(g, edge...) end
MetricTools.add!(g::MetricGraph, lines::Array{Line}) =
    for line in lines add!(g, line...) end

"""
Gets an array of all edges in the graph
"""
get_edges(g::MetricGraph)::Array{MetaGraphs.Edge} =
    [e for e in MetaGraphs.edges(g.graph)]

"""
Removes an edge
"""
MetricTools.remove!(g::MetricGraph, v1::Integer, v2::Integer) =
    LightGraphs.rem_edge!(g.graph, v1, v2)
MetricTools.remove!(g::MetricGraph, p1::Point, p2::Point) =
    LightGraphs.rem_edge!(g.graph, get_vertex(g, p1), get_vertex(g, p2))
#endregion edges


#region vertices
"""
Gets an array of vertices in the graph
"""
get_vertices(g::MetricGraph)::Array{Integer} =
    [v for v in MetaGraphs.vertices(g.graph)]

"""
Gets the index of a vertex
"""
get_vertex(g::MetricGraph, point::Point)::Integer = g.pointids[point]

"""
Gets the neighbors of a vertex
"""
get_neighbors(g::MetricGraph, vertex::Integer)::Array{Integer} = 
    MetaGraphs.outneighbors(g.graph, vertex)
get_neighbors(g::MetricGraph, point::Point)::Array{Point} =
    [get_point(g, n) for n in get_neighbors(g, get_vertex(g, point))]
#endregion vertices


#region distances
"""
Builds the KNN datastruct if not already built
"""
nnbuild!(g::MetricGraph) = if !g.knn.built build!(g.knn) end

"""
Forces rebuild the KNN datastruct
"""
nnrebuild!(g::MetricGraph) = build!(g.knn)

"""
Gets pre-computed distance for edge
"""
MetricTools.distance(g::MetricGraph, e::MetaGraphs.Edge)::Union{Nothing, Float64} =
    MetaGraphs.get_prop(g.graph, e, :weight)

"""
Gets pre-computed distance for two points
"""
MetricTools.distance(g::MetricGraph, p1::Point, p2::Point) =
    distance(g, MetaGraphs.Edge(get_vertex(g, p1), get_vertex(g, p2)))

"""
Returns the length of a path of edges
"""
MetricTools.distance(g::MetricGraph, path::Array{MetaGraphs.Edge})::Union{Float64, Nothing} =
    isempty(path) ? nothing : sum((e)->distance(g, e), path)

"""
Returns the precomputed length of a path of points
"""
MetricTools.distance(g::MetricGraph, path::Array{Point})::Union{Float64, Nothing} =
    isempty(path) ? nothing : sum((l)->distance(g, l[1], l[2]), pathlines(path))

"""
Gets distance between two points using the metric
"""
MetricTools.metric(g::MetricGraph, p1::Point, p2::Point)::Union{Nothing, Float64} = 
    g.metric(p1, p2)

"""
Returns the k nearest neighbor ids to a given query point using an R-Tree
"""
knnids(g::MetricGraph, point::Point, k::Integer)::Array{Integer} =
    nearestids(g.knn, point, k)

"""
Returns the k nearest neighbors to a given query point using an R-Tree
"""
knn(g::MetricGraph, point::Point, k::Integer)::Array{Point} =
    nearest(g.knn, point, k)

"""
Returns all points in a radius
"""
MetricTools.inradius(g::MetricGraph, point::Point, r::Float64; ordered=true::Bool)::Array{Point} =
    inradius(g.knn, point, r; ordered=ordered)
#endregion distances


#region search
"""
Runs an A* Search over the graph and returns the path
"""
function astar(g::MetricGraph, start::Point, goal::Point)::Array{Point}
    
    if !(contains(g, start) && contains(g, goal)) return [] end
    vstart, vgoal = get_vertex(g, start), get_vertex(g, goal)

    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(g.graph)

    # preccompute heuristic values
    heuristics::Dict{Integer, Float64} = Dict(
        v => g.metric(get_point(g, v), goal) for v in get_vertices(g))

    heuristic::Function = (v)->heuristics[v]
    edges::Array{MetaGraphs.Edge} = MetaGraphs.a_star(g.graph, vstart, vgoal, weights, heuristic)
    return get_points(g, edges)
end


function calculateKey(node, g_score, rhs, heuristic)
    return [min(g_score[node], rhs[node])+ heuristic(node), min(g_score[node], rhs[node])]
end

"""
Runs an  LPA* Search over the graph and returns the path
"""
function lp_astar_initialize(m_g::MetricGraph, start::Point, goal::Point)
    Incremental_factor = 20000
    s, g = get_vertex(m_g, start), get_vertex(m_g, goal)
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)

    # distmx::LightGraphs.AbstractMatrix{T}=MetaGraphs.weights(m_g.graph),
    # preccompute heuristic values
    # heuristics::Dict{Integer, Float64} = Dict(
    #     v => g.metric(get_point(g, v), goal) for v in get_vertices(g))
    # heuristic::Function = (v)->heuristics[v]

    heuristic::Function = (v)-> m_g.metric(get_point(m_g, v), goal)  ### shouldn't pre-compute, 
                                                             ### initial state doesn't have all points
    
    E = LightGraphs.Edge{eltype(m_g.graph)}

    # if we do checkbounds here, we can use @inbounds in a_star_impl!
    checkbounds(weights, Base.OneTo(LightGraphs.nv(m_g.graph)), Base.OneTo(LightGraphs.nv(m_g.graph)))

    pq = PriorityQueue{Integer, Array{Float64}}() # change priority to be 2d 

    g_score = fill(Inf, LightGraphs.nv(m_g.graph)*Incremental_factor)
    # g_score[s] = 0

    rhs = fill(Inf, LightGraphs.nv(m_g.graph)*Incremental_factor) ## add rhs
    rhs[s] = 0 # start is the only one inconsistant initally
    
    # f_score = fill(Inf, LightGraphs.nv(m_g.graph)) ## todo: need this?
    # f_score[s] = heuristic(s)

    came_from = -ones(Integer, LightGraphs.nv(m_g.graph)*Incremental_factor)
    came_from[s] = s

    enqueue!(pq, s, calculateKey(s, g_score, rhs, heuristic))
    # return get_points(g, edges)
    return m_g, s, g, pq, g_score, came_from, heuristic, rhs
end



# Recalculates rhs for a node and removes it from the PriorityQueue
# if the node becomes locally inconsistent, it is re-inserted into the PriorityQueue with new key
function updateNode_opt(node, s, g_score, rhs, pq, heuristic, m_g, came_from)
    if (g_score[node] != rhs[node]) 
        # println("add")
        pq[node] = calculateKey(node, g_score, rhs, heuristic)
    else
        if node in keys(pq) 
            delete!(pq, node)
        end
    end
end


   


# Recalculates rhs for a node and removes it from the PriorityQueue
# if the node becomes locally inconsistent, it is re-inserted into the PriorityQueue with new key
function updateNode(node, s, g_score, rhs, pq, heuristic, m_g, came_from)
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    if node != s
        rhs[node] = Inf
        ##Todo: Instead of using the neighbor, should use predecessor?
        # for neighbor in LightGraphs.outneighbors(m_g.graph, node) 
        #     rhs[node] = min(rhs[node], g_score[neighbor]+ weights[node, neighbor])
        # end
        for neighbor in LightGraphs.outneighbors(m_g.graph, node)
            if neighbor == node continue end
            temp = g_score[neighbor]+ weights[node, neighbor]
            if temp < rhs[node]
                came_from[node] = neighbor
                rhs[node] = temp
            end
        end
        # if node in keys(pq) 
        #     delete!(pq, node)
        # end
        # if g_score[node] != rhs[node]
        #     pq[node] = calculateKey(node, g_score, rhs, heuristic)
        # end
        
        if g_score[node] != rhs[node]
            pq[node] = calculateKey(node, g_score, rhs, heuristic)
        else
            if node in keys(pq) 
                delete!(pq, node)
            end
        end
        
    end
end

function get_top_key(pq)
    if isempty(pq) return [Inf, Inf]  end
    # return peek(pq)[2]
    return peek(pq)[2]+[-0.00001, 0]
end
        
function lp_astar_compute_shortest_path(
    m_g, # the graph
    start::Integer,
    goal::Integer, # the end vertex
    pq, # an initialized heap containing the active vertices
    g_score, # a vector holding g scores for each node
    came_from, # a vector holding the parent of each node in the A* exploration
    heuristic,
    rhs
    )::Tuple{Array{Point}, Float64}
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    E = LightGraphs.edgetype(m_g.graph)

    total_path::Array{MetaGraphs.Edge}  = Vector{E}()
    println("goes in this function ")
    counter = 0
    

    @inbounds while true
        if  (get_top_key(pq) < calculateKey(goal,g_score, rhs, heuristic)) || (rhs[goal] != g_score[goal])
            nothing
        else
            curr_idx = goal
            restart = false
            while came_from[curr_idx] != curr_idx
                # println(came_from[curr_idx])
                # pushfirst!(total_path, E(came_from[curr_idx], curr_idx))
                curr_idx = came_from[curr_idx]
                if g_score[curr_idx] != rhs[curr_idx]
                    g_score[goal] = Inf
                    rhs[goal] = Inf
                    restart = true
                    break
                end
            end
            if !restart break end
        end

    
    # @inbounds while (get_top_key(pq) < calculateKey(goal,g_score, rhs, heuristic)) || (rhs[goal] != g_score[goal])
        # println("---------------------------")
        # println("rhs[goal]: ", rhs[goal])
        # println("g_score[goal]: ", g_score[goal])
        # println("get goal key: ", calculateKey(goal,g_score, rhs, heuristic))
        # println("----------------------------")
        # println("current top key: ", peek(pq)[2])
        current = dequeue!(pq)
        # println("current key is: ", current, " g: ", g_score[current],  " rhs: ", rhs[current])

        if g_score[current] > rhs[current]
            g_score[current] = rhs[current]
            ##Todo: Instead of using the neighbor, should use successor
            for neighbor in LightGraphs.outneighbors(m_g.graph, current) 
                if neighbor == current continue end
                updateNode(neighbor, start, g_score, rhs, pq, heuristic, m_g, came_from)
                counter += 1
            end
        else
            g_score[current] = Inf
            updateNode(current,start, g_score, rhs, pq, heuristic, m_g, came_from)
            counter += 1
            ##Todo: Instead of using the neighbor, should use successor
            for neighbor in LightGraphs.outneighbors(m_g.graph, current) 
                if neighbor == current continue end
                updateNode(neighbor, start, g_score, rhs, pq, heuristic, m_g, came_from)
                counter += 1
            end
        end
    end


    # E = edgetype(g)
    # curr_idx = goal
    # while came_from[curr_idx] != curr_idx
    #     println(came_from[curr_idx])
    #     # pushfirst!(total_path, E(came_from[curr_idx], curr_idx))
    #     curr_idx = came_from[curr_idx]
    # end

    # println("the total path is")
    println("the num of calling update node is: ", counter)



    if came_from[goal] != -1 && g_score[goal] != Inf
        LightGraphs.reconstruct_path!(total_path, came_from, goal, m_g.graph)
    else
        return [], Inf
    end

    return get_points(m_g, total_path), g_score[goal]
end


function lp_astar_compute_shortest_path_opt(
    m_g, # the graph
    start::Integer,
    goal::Integer, # the end vertex
    pq, # an initialized heap containing the active vertices
    g_score, # a vector holding g scores for each node
    came_from, # a vector holding the parent of each node in the A* exploration
    heuristic,
    rhs;
    verbose=false
    )::Tuple{Array{Point}, Float64}
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    E = LightGraphs.edgetype(m_g.graph)

    total_path::Array{MetaGraphs.Edge}  = Vector{E}()
    if verbose println("Goes in function lp_astar_compute_shortest_path_opt") end
    counter = 0
    ccc = 0
    @inbounds while true

        if  (get_top_key(pq) < calculateKey(goal,g_score, rhs, heuristic)) || (rhs[goal] > g_score[goal])
            nothing
        else
            curr_idx = goal
            restart = false
            while came_from[curr_idx] != curr_idx
                # pushfirst!(total_path, E(came_from[curr_idx], curr_idx))
                curr_idx = came_from[curr_idx]
                if g_score[curr_idx] != rhs[curr_idx] || curr_idx == -1
                    # g_score[goal] = Inf
                    # rhs[goal] = Inf
                    restart = true
                    break
                end
            end
            if !restart break end
        end

    # @inbounds while (get_top_key(pq) < calculateKey(goal,g_score, rhs, heuristic)) || (rhs[goal] > g_score[goal])
        ccc += 1
        if isempty(pq) break end ##!add this line
        current = peek(pq)[1]
        if g_score[current] > rhs[current]
            g_score[current] = rhs[current]
            delete!(pq, current)
            ##Todo: Instead of using the neighbor, should use successor
            for neighbor in LightGraphs.outneighbors(m_g.graph, current) 
                if neighbor == current continue end
                temp = g_score[current]+ weights[current, neighbor] 
                if temp < rhs[neighbor]
                    came_from[neighbor] = current
                    rhs[neighbor] = temp
                    updateNode_opt(neighbor, start, g_score, rhs, pq, heuristic, m_g, came_from)
                    counter += 1
                end
            end
        else
            g_score[current] = Inf
            if current != start && came_from[current] == current
                println("wrong!!!!")
            end
            updateNode_opt(current,start, g_score, rhs, pq, heuristic, m_g, came_from)
            counter += 1
            ##Todo: Instead of using the neighbor, should use successor
            for neighbor in LightGraphs.outneighbors(m_g.graph, current) 
                if neighbor == current continue end
                if neighbor != start && came_from[neighbor] == current
                    rhs[neighbor] = Inf
                    came_from[neighbor] = -1
                    for nn in LightGraphs.outneighbors(m_g.graph, neighbor)
                        if nn == neighbor continue end
                        temp = g_score[nn]+ weights[nn, neighbor]
                        if temp < rhs[neighbor]
                            came_from[neighbor] = nn
                            rhs[neighbor] = temp
                        end
                    end
                end
                updateNode_opt(neighbor, start, g_score, rhs, pq, heuristic, m_g, came_from)
                counter += 1
            end
        end
    end

    # E = edgetype(g)
    # curr_idx = goal
    # while came_from[curr_idx] != curr_idx
    #     println(came_from[curr_idx])
    #     # pushfirst!(total_path, E(came_from[curr_idx], curr_idx))
    #     curr_idx = came_from[curr_idx]
    # end

    # println("the total path is")
    # g_score[goal] = rhs[goal]
    if verbose
        println("the num of calling update node is: ", counter)
        println("goal is: ", goal)
        println("came from goal" , came_from[goal])
        println(g_score[goal])
        println("rhs: ", rhs[goal])
    end


    if came_from[goal] != -1 && rhs[goal] != Inf
        LightGraphs.reconstruct_path!(total_path, came_from, goal, m_g.graph)
    else
        return [], Inf
    end
    if verbose println("total path is: ", total_path) end
    return get_points(m_g, total_path), rhs[goal]
end



"""
Runs an changed A* Search over the graph and returns the path
"""
function changed_astar(g::MetricGraph, start::Point, goal::Point, add_all_cells_contain_p::Function, n_leaves::Integer)::Array{Point}
    vstart, vgoal = get_vertex(g, start), get_vertex(g, goal)
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(g.graph)

    # preccompute heuristic values
    # heuristics::Dict{Integer, Float64} = Dict(
    #     v => g.metric(get_point(g, v), goal) for v in get_vertices(g))
    # heuristic::Function = (v)->heuristics[v]

    heuristic::Function = (v)-> g.metric(get_point(g, v), goal)  ### shouldn't pre-compute, 
                                                             ### initial state doesn't have all points
    edges::Array{MetaGraphs.Edge} = changed_a_star(g, vstart, vgoal, n_leaves, add_all_cells_contain_p,weights, heuristic)
    
    return get_points(g, edges)
end


"""
Changed a star to construct with cells
"""
function changed_a_star(m_g,  # the g
    s::Integer,                       # the start vertex
    t::Integer, 
    n_leaves::Integer,
    add_all_cells_contain_p::Function,                     # the end vertex
    distmx::LightGraphs.AbstractMatrix{T}=MetaGraphs.weights(m_g.graph),
    heuristic::Function=n -> zero(T)
    ) where {T}

    E = LightGraphs.Edge{eltype(m_g.graph)}

    # if we do checkbounds here, we can use @inbounds in a_star_impl!
    checkbounds(distmx, Base.OneTo(LightGraphs.nv(m_g.graph)), Base.OneTo(LightGraphs.nv(m_g.graph)))

    open_set = PriorityQueue{Integer, T}()
    enqueue!(open_set, s, 0)

    closed_set = zeros(Bool, LightGraphs.nv(m_g.graph)*n_leaves+1)

    g_score = fill(Inf, LightGraphs.nv(m_g.graph)*n_leaves+1)
    g_score[s] = 0

    f_score = fill(Inf, LightGraphs.nv(m_g.graph)*n_leaves+1)
    f_score[s] = heuristic(s)

    came_from = -ones(Integer, LightGraphs.nv(m_g.graph)*n_leaves+1)
    came_from[s] = s

    changed_a_star_impl!(m_g, s, t, open_set, closed_set, g_score, f_score, came_from, distmx, heuristic, add_all_cells_contain_p)
end

function changed_a_star_impl!(m_g, # the graph
    start,
    goal, # the end vertex
    open_set, # an initialized heap containing the active vertices
    closed_set, # an (initialized) color-map to indicate status of vertices
    g_score, # a vector holding g scores for each node
    f_score, # a vector holding f scores for each node
    came_from, # a vector holding the parent of each node in the A* exploration
    distmx,
    heuristic,
    add_all_cells_contain_p::Function
    )

    E = LightGraphs.edgetype(m_g.graph)
    total_path = Vector{E}()

    @inbounds while !isempty(open_set)
        current = dequeue!(open_set)
        if current == goal
            LightGraphs.reconstruct_path!(total_path, came_from, current, m_g.graph)
            return total_path
        end

        add_all_cells_contain_p(current, m_g)

        closed_set[current] = true

        for neighbor in LightGraphs.outneighbors(m_g.graph, current)
            closed_set[neighbor] && continue

            tentative_g_score = g_score[current] + distmx[current, neighbor]

            if tentative_g_score < g_score[neighbor] 
                g_score[neighbor] = tentative_g_score
                priority = tentative_g_score + heuristic(neighbor)
                open_set[neighbor] = priority
                # enqueue!(open_set, neighbor, priority)
                came_from[neighbor] = current
            end
        end
    end
    return total_path
end

"""
Runs Djikstra's Algorithm over the graph and returns the state, using a start vertex
"""
dijkstras(g::MetricGraph, startv::Integer; allpaths=false, trackvertices=false
        )::LightGraphs.DijkstraState =
    LightGraphs.dijkstra_shortest_paths(
        g.graph, [startv], MetaGraphs.weights(g.graph);
        allpaths=allpaths, trackvertices=trackvertices)
dijkstras(g::MetricGraph, start::Point;
        allpaths=false, trackvertices=false)::LightGraphs.DijkstraState =
    dijkstras(g, get_vertex(g, start);
        allpaths=allpaths, trackvertices=trackvertices)

"""
Gets the distance for a point after a djistras search
"""
dijkstradist(g::MetricGraph, goal::Point, dists::Array{<:Real})::Float64 =
    dists[get_vertex(g, goal)]

"""
Gets the path for a point after a djistras search
"""
function dijkstrapath(g::MetricGraph, start::Point, goal::Point, parents::Array)
    path::Array{Point} = []
    current::Integer = get_vertex(g, goal)
    startv::Integer = get_vertex(g, start)
    while current > 0 && current != startv
        pushfirst!(path, get_point(g, current))
        current = parents[current]
    end

    if current == startv
        pushfirst!(path, start)
        return path
    end
    
    return []
end

"""
Runs Floyd Warshall over the graph and returns the state
"""
function floyd_warshall(g::MetricGraph)::LightGraphs.FloydWarshallState
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(g.graph)
    LightGraphs.floyd_warshall_shortest_paths(g.graph, weights)
end
#endregion search


#region utils
"""
Returns true if the graph already contains this point
"""
MetricTools.contains(g::MetricGraph, point::Point)::Bool =
    haskey(g.pointids, point)

"""
Returns the corresponding lines from a list of edges
"""
get_lines(g::MetricGraph, edges::Array{MetaGraphs.Edge})::Array{Line} =
    [(get_point(g, e.src), get_point(g, e.dst)) for e in edges]

"""
Returns the lines from all edges of the graph
"""
get_lines(g::MetricGraph)::Array{Line} =
    [(get_point(g, e.src), get_point(g, e.dst)) for e in get_edges(g)]

"""
Returns the corresponding points from a list of edges
"""
function get_points(g::MetricGraph, edges::Array{MetaGraphs.Edge})::Array{Point}
    if isempty(edges) return [] end
    return vcat([get_point(g, first(edges).src)], [get_point(g, e.dst) for e in edges])
end

"""
Gets the degree of a point in the metric graph
"""
degree(g::MetricGraph, vertex::Integer) = MetaGraphs.degree(g.graph, vertex)
degree(g::MetricGraph, point::Point) = degree(g, get_vertex(g, point))

"""
Plots a metric graph using PyPlot
"""
function MetricTools.plot(g::MetricGraph;
        linecolor="grey"::String,
        ax=PyCall.PyNULL()::PyCall.PyObject,
        plotpoints::Bool=false,
        progress::Bool=true)
    if plotpoints
        points::Array{Point} = get_points(g)
        plot(points; ax=ax)
    end

    edges::Array{LightGraphs.Edge} = get_edges(g)
    lines::Array{Line} = get_lines(g, LightGraphs.Edge[e for e in edges if distance(g, e) != 0])
    plot(lines; ax=ax, color=linecolor, alpha=0.25, progress=progress)
end
#endregion utils

end
