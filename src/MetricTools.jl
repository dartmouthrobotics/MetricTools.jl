#=
 * File: MetricTools.jl
 * Project: MetricTools
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 10th April 2020 6:04:24 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module MetricTools

import PyCall

export
    add!,
    Bound,
    contains,
    distance,
    distfunc,
    inradius,
    KNNs,
    Geometry,
    Line,
    Line2D,
    Math,
    metric,
    pathlines,
    Point,
    Point2D,
    Polygon,
    Polygon2D,
    plot,
    plotfunc,
    remove!,
    connect!,
    @mustimplement,
    GJKTools,
    EarClipping

#region typedefs
const Point = Array{Float64}
const Point2D = Tuple{Float64, Float64}
const Polygon = Array{Point}
const Polygon2D = Array{Point2D}
const Line = Tuple{Point, Point}
const Line2D = Tuple{Point2D, Point2D}

struct Bound
    lo::Float64
    hi::Float64
end

Base.in(p::Point, bs::Array{Bound}) =
    all([bs[i].lo <= p[i] <= bs[i].hi for i in eachindex(p)])
    
#endregion typedefs

#region functions
function plot() end
function plotfunc() end
function add!() end
function remove!() end
function contains() end
function inradius() end
function distance() end
function distfunc() end
function connect!() end

"""
Gets the lines from a path of points
"""
pathlines(path::Array{Point})::Array{Line} =
    [(path[i], path[i + 1]) for i in firstindex(path):lastindex(path)-1]

"""
Returns the metric length of a path of points
"""
metric(metricf::Function, path::Array{Point}) =
    isempty(path) ? nothing : sum((l)->metricf(l[1], l[2]), pathlines(path))
#endregion functions

#region macros
"""
From the Graphics package
"""
macro mustimplement(sig)
    fname = sig.args[1]
    arg1 = sig.args[2]
    if isa(arg1,Expr)
        arg1 = arg1.args[1]
    end
    :($(esc(sig)) = error(typeof($(esc(arg1))),
                          " must implement ", $(Expr(:quote,sig))))
end
#endregion macros

include("./Visualization.jl")
include("./Math.jl")

include("./Geometry/Geometry.jl")
include("./Geometry/EarClipping.jl")
include("./Geometry/GJKTools.jl")

include("./KNNs.jl")
include("./MetricGraphs.jl")

end # module
