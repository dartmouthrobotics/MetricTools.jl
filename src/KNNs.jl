#=
 * File: KNNs.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 19th April 2020 11:29:35 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module KNNs

#region imports
using DataStructures
import Distances
import LibSpatialIndex, NearestNeighbors

using ..MetricTools
#endregion imports

export KNN, BruteKNN, BallTreeKNN, inradius, nearest, nearestids, build!

abstract type KNN end

"""
Checks if the knn structure has been built / is updated
"""
function checkbuilt(knn::KNN)
    if !knn.built
        error("KNN structure not built (Call KNNs.build)")
    end
end

#region BruteKNN

"""
Brute force KNN using a priority queue
"""
mutable struct BruteKNN <: KNN
    dim::Integer
    points::Array{Point}
    metric::Distances.Metric
    built::Bool
    
    function BruteKNN(dim::Integer; points=nothing::Union{Array{Point}, Nothing}, metric=Distances.Euclidean()::Distances.Metric)
        if isnothing(points) points = [] end
        return new(dim, points, metric, true)
    end
end

"""
Inserts a point into the array of points for brute force KNN
"""
function MetricTools.add!(knn::BruteKNN, point::Point, id::Integer)
    if lastindex(knn.points) + 1 != id error("Invalid ID ($id).") end
    push!(knn.points, point)
end

"""
'Builds' the brute force KNN
"""
function build!(knn::BruteKNN)
    knn.built = true
end

"""
Returns the nearest ids using brute force
"""
function nearestids(knn::BruteKNN, point::Point, k)::Array{Integer}
    pq::PriorityQueue = PriorityQueue()
    for (i, n) in enumerate(knn.points)
        pq[i] = knn.metric(point, n)
    end
    return [dequeue!(pq) for _ in 1:k]
end

"""
Returns the k nearest neighbors to a given query point using brute force
"""
function nearest(knn::BruteKNN, point::Point, k::Integer)::Array{Point}
    pq::PriorityQueue = PriorityQueue()
    for n in knn.points
        pq[n] = knn.metric(point, n)
    end
    return [dequeue!(pq) for _ in 1:k]
end

"""
Returns the k nearest neighbors to a given query point using brute force
"""
function MetricTools.inradius(knn::BruteKNN, point::Point, r::Float64; ordered=true::Bool)::Array{Point}
    
    results::Array{Point} = []
    for n in knn.points
        if knn.metric(point, n) <= r push!(results, n) end
    end
    return results
end

#endregion BruteKNN

#region BallTreeKNN
"""
KNN using NearestNeighbors' BallTree
"""
mutable struct BallTreeKNN <: KNN
    dim::Integer
    balltree::NearestNeighbors.BallTree
    points::Matrix{Float64}
    metric::Distances.Metric
    built::Bool
    
    function BallTreeKNN(dim::Integer;
            points=Point[]::Array{Point},
            metric::Distances.Metric=Distances.Euclidean())
        pointsmat::Matrix{Float64} = fill(Float64, (dim, 0))
        for point in points pointsmat = hcat(pointsmat, point) end
        balltree::NearestNeighbors.BallTree = NearestNeighbors.BallTree(pointsmat, metric)
        return new(dim, balltree, pointsmat, metric, true)
    end
end

"""
Inserts a point into the KDTree
"""
function MetricTools.add!(knn::BallTreeKNN, point::Point, id::Integer)
    if div(lastindex(knn.points), knn.dim) + 1 != id error("Invalid ID ($id), expected $(lastindex(knn.points) + 1).") end
    knn.points = hcat(knn.points, point)
    knn.built = false
end


"""
Builds the KDTree
"""
function build!(knn::BallTreeKNN)
    knn.balltree = NearestNeighbors.BallTree(knn.points, knn.metric)
    knn.built = true
end

"""
Returns the k nearest ids for an KDTree
"""
function nearestids(knn::BallTreeKNN, point::Point, k::Integer)::Array{Integer}
    checkbuilt(knn)
    idxs, dists = NearestNeighbors.knn(knn.balltree, point, k, true)
    return idxs
end

"""
Returns the k nearest neighbors to a given query point using brute force
"""
function nearest(knn::BallTreeKNN, point::Point, k::Integer)::Array{Point}
    checkbuilt(knn)
    return [[knn.points[d, i] for d in 1:knn.dim] for i in nearestids(knn, point, k)]
end

"""
Returns the k nearest neighbors to a given query point using brute force
"""
function MetricTools.inradius(knn::BallTreeKNN, point::Point, r::Float64; ordered=true::Bool)::Array{Point}
    checkbuilt(knn)
    idxs = NearestNeighbors.inrange(knn.balltree, point, r, ordered)
    return [[knn.points[d, i] for d in 1:knn.dim] for i in idxs]
end
#endregion BallTreeKNN

end
