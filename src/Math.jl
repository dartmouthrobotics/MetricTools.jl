#=
 * File: Math.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 21st April 2020 2:22:10 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module Math

#region imports
import LinearAlgebra
import NLopt

using ..MetricTools
#endregion imports

export optimize, polar2rn, rn2polar

"""
Converts polar point u relative to sphere (c, r) to x in Rn
x[0] = r * cos(u[0])
x[1] = r * sin(u[0]) * cos(u[1])
x[2] = r * sin(u[0]) * sin(u[1]) * cos(u[2])
...
x[n-1] = r * sin(u[0]) * ... * sin(u[n-2]) * cos(u[n-1])
x[n] = r * sin(u[0]) * ... * sin(u[n-2]) * sin(u[n-1])
https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates
"""
function polar2rn(u::Point, c::Point, r::Float64)::Point
    
    x = [
        r * cos(ui) * (i > 1 ? prod([sin(u[j]) for j in 1:(i-1)]) : 1) 
        for (i, ui) in enumerate(u)
    ]
    push!(x, r * prod([sin(ui) for ui in u]))
    return x .+ c
end

"""
Converts point x in Rn to polar point u relative to sphere (c, r)
vi = arccos(xi / (sqrt(sum(xj^2 for j in i:n))))
https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates 
"""
function rn2polar(x::Point, c::Point, r::Float64)::Point
    v::Point = x .- c
    u = [
        sqrt(sum([v[j] for j in 1:i]))
        for i in 1:length(v) - 1
    ]
    return u
end

"""
Optimizes a function
Wraps NLopt
"""
function optimize(optf::Function;
        start::Point,
        algorithm=:LN_COBYLA,
        xtol_rel=1e-3::Float64,
        constraintf::Function=(x, g) -> 0.,
        constraint::Float64=0.,
        maxeval::Integer=100,
        min=true::Bool)::Tuple{Point, Float64}

    opt = NLopt.Opt(:LN_COBYLA, length(start))
    opt.xtol_rel = 1e-3
    opt.maxeval = maxeval

    if min
        opt.min_objective = optf
    else
        opt.max_objective = optf
    end
    
    # NLopt.inequality_constraint!(opt, constraintf, constraint)
    
    (minf, minx, ret) = NLopt.optimize(opt, start)
    return minx, minf
end

"""
Creates an array of n points between p1 and p2 using linear interpolation
"""
function interpolate(p1::Point, p2::Point, n::Integer)::Array{Point}
    ranges = [range(p1[d]; stop=p2[d], length=(n+1)) for d in eachindex(p1)]
    return [[r[i] for r in ranges] for i in 1:(n+1)]
end
end
