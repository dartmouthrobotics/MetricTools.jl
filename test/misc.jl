#=
 * File: misc.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 9th December 2019 4:17:02 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
import PyPlot
import Distances

if false    # fix linter
    include("../src/MetricTools.jl")
    using 
        .MetricTools,
        .MetricTools.Visualization
end

using
    MetricTools,
    MetricTools.Visualization

#endregion imports

function testvis()
    println("Testing visualization...")
    PyPlot.close("all")

    l2(p) = sqrt(sum((p - [0, 1]).^2))
    l1(p) = sum(p)
    
    PyPlot.subplots()
    plot(l2; cmin=0.0, cmax=1.5, bounds=[Bound(0, 1), Bound(0, 2)])
    PyPlot.subplots()
    plot(l1, l2; cmin=0.0, cmax=1.5)
    return true
end

function testmp4()
    FPRM_IMG_DIR = "data/output/images/fprm/"
    FPRM_VID_DIR = "data/output/videos/fprm/"
    compilemp4(FPRM_IMG_DIR; outdir=FPRM_VID_DIR, outfile="fprm.mp4")
end

function testline()
    lines::Array{Line} = [([0, 0], [1, 1])]
    plot(lines; alpha=0.5)
    return true
end

@testset "Misc Tests" begin
    @test testvis()
    @test testline()
    # testmp4()
end
