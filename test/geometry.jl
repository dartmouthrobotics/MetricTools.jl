#=
 * File: geometry.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 4th February 2020 8:20:50 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
import PyPlot

if false
    include("../src/MetricTools.jl")
    using .MetricTools
    using .MetricTools.Geometry
end

using MetricTools
using MetricTools.Geometry
#endregion imports

function pointinpoly()::Bool
    polygon::Polygon = [
        [1, 1],
        [1, 2],
        [2, 2],
        [2, 1]
    ]
    p1::Point = [1, 1]
    @test contains(polygon, p1; onedge=true)
    @test !contains(polygon, p1; onedge=false)

    p2::Point = [1.1, 1.1]
    @test contains(polygon, p2; onedge=true)
    @test contains(polygon, p2; onedge=false)

    p3::Point = [0.9, 0.9]
    @test !contains(polygon, p3; onedge=true)
    @test !contains(polygon, p3; onedge=false)
    return true
end

function testcolinear()::Bool
    l1::Line = ([0, 0], [1, 1])
    l2::Line = ([1, 1], [2, 2])
    l3::Line = ([1, 1], [2, 1])
    l4::Line = ([0, 1], [1, 2])
    @test iscolinear(l1, l2)
    @test !iscolinear(l1, l3)
    @test !iscolinear(l1, l4)

    l5::Line = ([0.1, 0.1], [0.4, 0.1])
    l6::Line = ([0.1, 0.1], [0.4, 0.1])
    @test iscolinear(l5, l6)
    return true
end

function lineintersect()::Bool
    l1::Line = [0, 0],
         [1, 1]
    
    l2::Line = [0, 1],
         [1, 0]
    
    l3::Line = [0.1, 1], 
         [1.1, 0]
    
    @test intersects(l1, l2)
    @test !intersects(l2, l3)
    return true
end

function fuzztest()::Bool
    fig, ax = PyPlot.subplots()
    lines::Array{Line} = [(rand(2), rand(2)) for i in 1:10]
    intersecting = Set()
    for (l1, l2) in Iterators.product(lines, lines)
        if l1 == l2 continue end
        if intersects(l1, l2) push!(intersecting, l1, l2) end
    end
    plot(lines; ax=ax)
    intlist::Array{Line} = [i for i in intersecting]
    plot(intlist; ax=ax, color="red")
    return true
end

function pointonline()
    line::Line = ([0, 0], [1, 1])
    point::Point = [1, 1]
    @test !contains(line, point; onedge=false)
    @test contains(line, point; onedge=true)
    return true
end

function lineonline()
    line1::Line = ([0, 0], [1, 1])
    line2::Line = ([1, 1], [1, 0])
    @test !intersects(line1, line2; onedge=false)
    @test intersects(line1, line2; onedge=true)
    return true
end

function testintersection()
    lines::Array{Line} = 
        [([1.0, 1.0], [0.25, 0.8]), ([0.2, 0.85], [0.2, 0.2])]
    
    display(intersection(lines[1], lines[2]))
    return true
end

function polygonintersect()
    println("Testing polygon intersections...")
    fig, ax = PyPlot.subplots()
    elbow::Polygon = [[0.1, 0.2], [0.1, 0.3], [0.3, 0.3], [0.3, 0.1], [0.2, 0.1], 
    [0.2, 0.2]]
    polygons::Array{Polygon} = [
        elbow,
        elbow .+ [[0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2], [0.2, 0.2]]
    ]
    lines::Array{Line} = [
        ([0, 0], [0, 0.1]),
        ([0.1, 0.2], [0.2, 0.1]),
        ([0.0, 0.25], [0.1, 0.25]),
        ([0.2, 0.1], [0.3, 0.1]),
        ([0., 0.], [0.6, 0.6]),
        ([0.4, 0.3], [0.5, 0.5]),
        ([0.35, 0.4], [0.35, 0.5]),
    ]
    plot(polygons; ax=ax)
    
    # for no edge
    collisions::Array{Line} = []
    free::Array{Line} = []
    for line in lines
        println("\nline: $line")
        if intersects(polygons, line; onedge=false)
            push!(collisions, line)
        else
            push!(free, line)
        end
        
        plot(collisions; ax=ax, color="red")
        plot(free; ax=ax, color="blue")

    end
    
    @test lines[1] in free
    @test lines[2] in free
    @test lines[3] in free
    @test lines[4] in free
    @test lines[5] in collisions
    @test lines[6] in collisions
    @test lines[7] in collisions

    # for with edge
    collisions = []
    free = []
    for line in lines
        println("\nline: $line")
        if intersects(polygons, line; onedge=true)
            push!(collisions, line)
        else
            push!(free, line)
        end
    end
    
    plot(collisions; ax=ax, color="red")
    plot(free; ax=ax, color="blue")
    
    @test lines[1] in free
    @test lines[2] in collisions
    @test lines[3] in collisions
    @test lines[4] in collisions
    @test lines[5] in collisions
    @test lines[6] in collisions
    @test lines[7] in collisions
    return true
end

@testset "Geometry Tests" begin
    PyPlot.close("all")
    # @test testcolinear()
    # @test lineintersect()
    # @test fuzztest()
    # @test pointonline()
    # @test lineonline()
    # @test pointinpoly()
    @test polygonintersect()
end

