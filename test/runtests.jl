#=
 * File: runtests.jl
 * Project: MetricTools
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 16th October 2019 8:52:55 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test, Revise
#endregion imports

@testset "MetricTools Tests" begin
    println("Testing MetricTools...")
    include("$(@__DIR__)/geometry.jl")
    include("$(@__DIR__)/graphs.jl")
    include("$(@__DIR__)/math.jl")
    include("$(@__DIR__)/misc.jl")
end
