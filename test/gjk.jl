#=
 * File: gjk.jl
 * Project: test
 * File Created: Tuesday, 17th March 2020 4:10:17 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 18th March 2020 2:15:23 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using EnhancedGJK
using StaticArrays
using MetricTools

function test_gjk()
    polygon::Polygon = [
        [0.1, 0.1],
        [0.1, 0.2],
        [0.2, 0.2],
        [0.2, 0.1]
    ]
    point::Point = [0.1, 0.1]
    n::Integer = length(polygon)
    spolygon::SVector{n} = SVector{n}([SVector{2, Float64}(p) for p in polygon])
    spoint::SVector{2} = SVector{2}(point)

    result::GJKResult = gjk(spoint, spolygon)
    show(separation_distance(result))
end

test_gjk()
