#=
 * File: graphs.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Tuesday, 5th November 2019 4:21:01 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
import PyPlot
import Distances

if false    # fix linter
    include("../src/MetricTools.jl")
    using 
        .MetricTools,
        .MetricTools.MetricGraphs,
        .MetricTools.Visualization,
        .MetricTools.KNNs
end

using
    MetricTools,
    MetricTools.MetricGraphs,
    MetricTools.Visualization,
    MetricTools.KNNs

#endregion

function setup_graph()
    d::Int64 = 2
    g::MetricGraph = MetricGraph(2)

    points::Array{Point} = [
        [0, 0],     # start
        [1, 1],
        [2, 2],
        [1, 3],     # shortcut
        [3, 3],     # goal
    ]
    add!(g, points)

    edges::Array{Array{Integer}} = [
        [1, 4],
        [1, 2],
        [2, 3],
        [3, 5],
        [4, 5]
    ]
    add!(g, edges)
    return g, points
end

function test_graphs()::Bool
    g, points = setup_graph()

    for point in points
        v = get_vertex(g, point)
        graphpt = get_point(g, v)
        if point != graphpt return false end
    end

    fig, ax = PyPlot.subplots()
    plot(g; ax=ax)
    path = astar(g, first(points), last(points))
    plot(path; ax=ax, color="red")
    dist = distance(g, path)

    nns = knn(g, points[1], 3)
    deg::Integer = degree(g, first(points))
    @test deg == 2
    plot(nns; ax=ax, color="red")
    return true
end

function test_knn()::Bool
    d::Int64 = 2
    gs::Array{MetricGraph} = [
        MetricGraph(d; knntype=BruteKNN),
        MetricGraph(d; knntype=LSIRTreeKNN),
        MetricGraph(d; knntype=NNKDTreeKNN),
    ]
    points::Array{Point} = [
        rand(d) for i in 1:100
    ]
    k = 5
    qp::Point = [0, 0]

    for g in gs
        println(typeof(g.knn))
        println("Adding points...")
        @time add!(g, points)
        println("Building KNN...")
        @time nnbuild!(g)
        println("Calculating KNN...")
        @time res = knn(g, qp, k)
        println(res)
        println("Plotting...")
        fig, ax = PyPlot.subplots()
        plot(g; ax=ax)
        plot(res; ax=ax, color="red")
        println()
    end
    return true
end

function test_inradius()::Bool
    d::Int64 = 2
    gs::Array{MetricGraph} = [
        MetricGraph(d; knntype=BruteKNN),
        MetricGraph(d; knntype=LSIRTreeKNN),
        MetricGraph(d; knntype=NNKDTreeKNN),
    ]
    points::Array{Point} = [
        rand(d) for i in 1:5000
    ]
    k = 5
    qp::Point = [0.5, 0.5]
    r = 0.2

    for g in gs
        println(typeof(g.knn))
        println("Adding points...")
        @time add!(g, points)
        println("Building KNN...")
        @time nnbuild!(g)
        println("Calculating in radius...")
        @time res = inradius(g, qp, r)
        println("Plotting...")
        fig, ax = PyPlot.subplots()
        plot(g; ax=ax)
        plot(res; ax=ax, color="red")
        plot(qp, r; ax=ax)
        println()
    end
    return true
end

function test_metric()::Bool
    d::Int64 = 2
    g1::MetricGraph = MetricGraph(d)
    g2::MetricGraph = MetricGraph(d)
    println("\nTesting metrics...")

    points::Array{Point} = [
        rand(2) for i in 1:100000
    ]
    @time add!(g1, points)
    @time add!(g2, points)
    println("Done adding points.")

    goal::Point = [0, 0]
    function calc_dists(g::MetricGraph)
        dict = Dict(v => g.metric(get_point(g, v), goal) for v in get_vertices(g))
    end
    println("Time for g1:")
    @time calc_dists(g1)
    println("Time for g2:")
    @time calc_dists(g2)
    return true
end

function test_performance()::Bool
    d::Int64 = 2
    g::MetricGraph = MetricGraph(d)
    
    points::Array{Point} = [
        rand(2) for i in 1:100000
    ]
    println("\nGetting performance times for adding and getting points")
    @time add!(g, points)
    @time get_points(g)
    @time get_vertices(g)
    @time get_lines(g)
    return true
end

function test_neighbors()::Bool
    g, points = setup_graph()
    testpoint::Point = [0, 0]
    ns = get_neighbors(g, testpoint)
    println(ns)
    return true
end

function test_distances()::Bool
    g, points = setup_graph()
    dist = distance(g, points[1], points[2])
    return true
end

@testset "Metric Graph Tests" begin
    PyPlot.close("all")
    @test test_neighbors()
    @test test_graphs()
    @test test_metric()
    @test test_distances()
    @test test_knn()
    @test test_inradius()
    # @test test_performance()
end
