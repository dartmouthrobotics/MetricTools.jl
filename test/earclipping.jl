#=
 * File: earclipping.jl
 * Project: test
 * File Created: Tuesday, 17th March 2020 4:26:34 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 18th March 2020 2:23:36 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools, MetricTools.Geometry
using MetricSpaces.Planar

function test_earclip()
    space::Space = PlanarSpace(;filename="data/input/spaces/planar/world-maze.json")
    poly1::Polygon = last(space.obstacles)
    plot(space)
    
    clipped::Array{Polygon} = earclip(poly1)
    plot(clipped)
end

test_earclip()
