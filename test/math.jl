#=
 * File: math.jl
 * Project: common
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 11th November 2019 8:09:22 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
import Distances
import NLopt

if false    # fix linter
    include("../src/MetricTools.jl")
    using 
        .MetricTools,
        .MetricTools.Math
end

using
    MetricTools,
    MetricTools.Math

#endregion imports

function test_polar2rn2D()::Bool
    c::Point = [0, 0]
    r::Float64 = 1
    u::Point = [pi / 2]

    p = polar2rn(u, c, r)
    return true
end

function test_polar2rn3D()::Bool
    c::Point = [0, 0, 0]
    r::Float64 = 1
    u::Point = [pi / 2, pi / 2]

    p = polar2rn(u, c, r)
    return true
end

function test_opt()
    target::Point = [1, 1]
    c::Point = [0, 0]
    r::Float64 = 1

    function optf(u::Array, grad::Array)
        x = polar2rn(convert(Point, u), c, r)
        return Distances.euclidean(x, target)
    end
    start::Point = [0]
    minu, minf = optimize(optf; start=start)

    println("Opt result: minu=$minu, minf=$minf")
    x = polar2rn(convert(Point, minu), c, r)
    utrue = pi / 4
    xtrue = polar2rn(convert(Point, [utrue]), c, r)
    dist = Distances.euclidean(xtrue, target)
    println("True result: $utrue, $dist")
    return true
end

function test_interp()::Bool
    p1::Point = [0, 0]
    p2::Point = [1, -1]
    interp::Array{Point} = Math.interpolate(p1, p2, 5)
    println(interp)
    return true
end

@testset "Math Tests" begin
    @test test_opt()
    @test test_polar2rn3D()
    @test test_opt()
    @test test_interp()
end

