using ProgressMeter

function pf(x::Float64, y::Float64)
    sleep(0.01)
    # println(x, y)
    return x + y
end

function test()
    xmin::Float64 = 0
    xmax::Float64 = 1
    xstep::Float64 = 0.1
    ymin::Float64 = 0
    ymax::Float64 = 2
    ystep::Float64 = 0.2
    
    x = xmin:xstep:xmax
    y = ymin:ystep:ymax

    println("plot x: $x")
    println("plot y: $y")

    z::Matrix{Float64} = @showprogress [pf(x0, y0) for y0 in y, x0 in x]
    display(z)
end

test()
